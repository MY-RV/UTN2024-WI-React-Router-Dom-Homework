import { AboutPage, HomePage, ProductsPage } from "./pages";
import { createBrowserRouter, redirect, redirectDocument } from "react-router-dom";

export default createBrowserRouter([
  {
    path: "/",
    loader: () => redirect("/home"),
  },
  {
    path: "/home",
    Component: HomePage,
  },
  {
    path: "/products",
    Component: ProductsPage,
  },
  {
    path: "/about",
    Component: AboutPage,
  },
])