import Layout from "../../layout";


export default function AboutPage() {
  return (
    <Layout>
      <h1 className="page-title">Products</h1>

      <p>
        <b>Creator:</b> Minor Yorseth Retana Vásquez
      </p>
      <p>
        <b>Email:</b> <a href="itsmyrvmail@gmail.com" target="_blank" type="Email">itsmyrvmail@gmail.com</a>
      </p>
      <p>
        <b>Birthday:</b> 2003/04/27
      </p>
      <p>
        <b>Resources:</b> <a href="https://dummyjson.com" target="_blank">https://dummyjson.com</a>
      </p>

      <br />

      <p>
        <b>Inspiration:</b> <a href="https://usehooks-ts.com/react-hook/use-fetch" target="_blank">UseHooks-ts (useFetch)</a>
      </p>
    </Layout>
  );
}