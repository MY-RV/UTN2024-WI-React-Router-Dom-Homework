import { OverlaidSpinner } from '../../../../components';
import { ReactNode } from 'react';
import './styles.css'


interface Props {
  children?: ReactNode;
  isLoading?: boolean;
}
export default function ProductsGrid({ children, isLoading = false }: Props) {
  return (
    <div className="products-grid">
      <div className="products-grid__container">
        {children}
      </div>
      {isLoading && <OverlaidSpinner />}
    </div>
  );

}