import { ProductOverview } from '../../../../models';
import './styles.css'


export default function ProductCard(props: ProductOverview) {
  return (
    <div className="product-card">
      <img className="product-card__thumbnail" src={props.thumbnail} alt="product thumbnail" />
      <div className="product-card__body">
        <h2 className="product-card__title">{props.title}</h2>
        <h3 className="product-card__category">{props.category}</h3>
        <p className="product-card__description">{props.description}</p>

        <p className="product-card__price">$ {props.price}</p>
      </div>
    </div>
  );

}