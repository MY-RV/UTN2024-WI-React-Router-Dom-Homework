import { useGetPagedProducts } from "../../lib/clients/dummyjson/use-get-paged-products";
import { ProductCard, ProductsGrid } from "./components";
import { DEFAULT_PAGE_SIZE } from "../../lib/constants";
import type { ProductOverview } from "../../models";
import { Paginator } from "../../components";
import { useEffect, useState } from "react";
import Layout from "../../layout";
import './styles.css'


export default function ProductsPage() {
  const [products, setProducts] = useState<ProductOverview[]>([]);
  const [dProducts, getProductsPage, pProducts] = useGetPagedProducts({
    pageNumber: 1, pageSize: DEFAULT_PAGE_SIZE
  }, true);

  useEffect(() => {
    if (dProducts.data) setProducts(dProducts.data);
    else if (dProducts.error) setProducts([]);
  }, [dProducts]);

  return (
    <Layout>
      <h1 className="page-title">Products</h1>

      <ProductsGrid isLoading={dProducts.loading}>{
        products.map((product) => <ProductCard key={product.id} {...product} />)
      }</ProductsGrid>

      <div className="products-page-paginator-wrapper">
        <Paginator
          currentPage={pProducts.pageNumber}
          totalPages={pProducts.totalPages}
          disabled={dProducts.loading}

          onPageChange={(pageNumber) => getProductsPage({
            pageNumber, pageSize: pProducts.pagesSize,
          })}
        />
      </div>
    </Layout>
  );
}