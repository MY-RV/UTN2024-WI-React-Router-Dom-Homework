import Layout from "../../layout";
import './styles.css';


export default function HomePage() {

  return (
    <Layout>
      <h1 className="page-title">Home</h1>

      <img className=" image-placeholder image-placeholder--1"  src='/image-placeholder.jpg' alt="img placeholder" />

      <p>
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Cum quibusdam, distinctio doloremque saepe enim facilis dicta maiores tempora hic corrupti explicabo recusandae deserunt labore voluptate eius debitis sunt repellendus amet!
        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Libero voluptatem praesentium quae, blanditiis cum reprehenderit nisi, nobis corporis culpa nam ipsa ipsam aut placeat! Assumenda nesciunt odio a enim qui?
      </p>

      <p>
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Cum quibusdam, distinctio doloremque saepe enim facilis dicta maiores tempora hic corrupti explicabo recusandae deserunt labore voluptate eius debitis sunt repellendus amet!
        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Libero voluptatem praesentium quae, blanditiis cum reprehenderit nisi, nobis corporis culpa nam ipsa ipsam aut placeat! Assumenda nesciunt odio a enim qui?
      </p>

      <img className=" image-placeholder image-placeholder--2"  src='/image-placeholder.jpg' alt="img placeholder" />
    </Layout>
  );
}