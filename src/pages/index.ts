export { default as ProductsPage } from './products/page';
export { default as AboutPage } from './about/page';
export { default as HomePage } from './home/page';
