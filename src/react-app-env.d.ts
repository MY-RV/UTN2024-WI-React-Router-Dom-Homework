/// <reference types="react-scripts" />

interface PaginationInfo {
  pagesSize: number;
  pageNumber: number;
  totalPages: number;
  totalItems: number;
}

type ReactChildren = string | JSX.Element | JSX.Element[] | (() => JSX.Element) | Element[]
