import { Navigation } from './components'
import './styles.css'

export default function Header() {
  return (
    <div className="app-header">
      <header className="app-header__element">
        <h1>
          Minor Retana
        </h1>
        <Navigation />
      </header>
    </div>
  )
}