import { NavLink } from "react-router-dom";
import './styles.css'

export default function Navigation() {
  function getClassNames({ isActive, isPending }: { isActive: boolean, isPending: boolean }) {
    return `app-navigation__link app-navigation__link--${isPending ? "pending" : isActive ? "active" : ""}`
  }

  return (
    <nav className="app-navigation">
      <NavLink to="/home" className={getClassNames}>
        Home
      </NavLink>
      <NavLink to="/products" className={getClassNames}>
        Products
      </NavLink>
      <NavLink to="/about" className={getClassNames}>
        About
      </NavLink>
    </nav>
  )

}