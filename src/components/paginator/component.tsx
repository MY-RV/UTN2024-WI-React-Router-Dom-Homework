
import './styles.css'


interface Props {
  disabled?: boolean,
  currentPage: number,
  totalPages: number,
  onPageChange?: (page: number) => void
}
export default function Paginator({ disabled, currentPage, totalPages, onPageChange }: Props) {
  return (
    <div className='app-paginator'>
      <button className='app-paginator__button'
        onClick={() => onPageChange && onPageChange(currentPage - 1)}
        disabled={disabled || currentPage <= 1}
      >Prev</button>

      <span className='app-paginator__info'>
        {currentPage} / {totalPages}
      </span>

      <button className='app-paginator__button'
        onClick={() => onPageChange && onPageChange(currentPage + 1)}
        disabled={disabled || currentPage >= totalPages}
      >Next</button>
    </div>
  )
}