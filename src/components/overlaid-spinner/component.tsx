
import './styles.css'

export default function OverlaidSpinner() {
  return (
    <div className='overlaid-spinner'>
      <span className='overlaid-spinner__spinner overlaid-spinner__spinner--slow overlaid-spinner__spinner--delayed'></span>
      <span className='overlaid-spinner__spinner overlaid-spinner__spinner--fast overlaid-spinner__spinner--delayed'></span>
      <span className='overlaid-spinner__spinner overlaid-spinner__spinner--slow'></span>
      <span className='overlaid-spinner__spinner overlaid-spinner__spinner--fast'></span>
    </div>
  )
}