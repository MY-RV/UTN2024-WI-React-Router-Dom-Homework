import { Header } from '../components';
import { ReactNode } from 'react';
import './styles.css';

interface Props {
  children?: ReactNode
}
export default function Layout({ children }: Props) {
  return (
    <>
      <Header />
      <main className="app-main">
        {children}
      </main>
    </>
  )

}