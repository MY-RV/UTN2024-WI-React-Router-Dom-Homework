import type { UseFetchState } from "../../http/core/use-fetch";
import type { ProductOverview } from "../../../models";
import { useDJClient } from "../../http/use-dj-client";
import { DEFAULT_PAGE_SIZE } from "../../constants";
import { useEffect, useState } from "react";

export interface ProductsRoot {
  products: ProductOverview[]
  total: number
  skip: number
  limit: number
}

interface UseGetPagedProductsArgs {
  pageNumber: number,
  pageSize: number,
}
export function useGetPagedProducts(
  args: UseGetPagedProductsArgs,
  initialFetch = false
): [UseFetchState<ProductOverview[]>, (args: UseGetPagedProductsArgs) => void, PaginationInfo] {
  const [{ pageNumber, pageSize }, setArgs] = useState(cleanArgs(args));
  const [totalPages, setTotalPages] = useState(0);

  const getOptions = ({ pageSize: ps, pageNumber: pn } = { pageSize, pageNumber }) => ({
    queryParams: { skip: ((pn - 1) * ps), limit: ps }
  });

  const [state, fetcher] = useDJClient.get<ProductsRoot>('/products', getOptions(), initialFetch);

  useEffect(() => {
    if (!state.data) return;
    setTotalPages(Math.ceil(state.data.total / pageSize));
  }, [state])

  return [
    {
      data: state.data?.products,
      loading: state.loading,
      error: state.error,
    },
    (args: UseGetPagedProductsArgs) => {
      args = cleanArgs(args);

      fetcher(getOptions(args));
      setArgs(args);
    },
    {
      pagesSize: pageSize,
      pageNumber,
      totalPages,
      totalItems: state.data?.total ?? 0,
    }
  ]
}

function cleanArgs(args: UseGetPagedProductsArgs) {
  return {
    pageSize: Math.max(args.pageSize ?? 0, DEFAULT_PAGE_SIZE),
    pageNumber: Math.max(args.pageNumber ?? 0, 1),
  }
}
