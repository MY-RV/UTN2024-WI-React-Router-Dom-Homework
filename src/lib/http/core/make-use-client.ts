import type { UseFetchMethod, UseFetchOptions, UseFetchState, UseFetchUrl } from "./use-fetch";
import { useFetch } from "./use-fetch";


export type UseClientHandler = <T>(url: UseFetchUrl, originalOptions: Omit<UseFetchOptions, 'method'> | null, initialFetch?: boolean)
  => [UseFetchState<T>, ((options: Omit<UseFetchOptions, 'method'>) => void)];
export type UseClientHook = Readonly<{
  get: UseClientHandler,
  put: UseClientHandler,
  post: UseClientHandler,
  delete: UseClientHandler,
}>

const CLIENT_METHODS: Readonly<UseFetchMethod[]> = Object.freeze(['GET', 'POST', 'PUT', 'DELETE']);

export const makeUseClient = (domain: string): UseClientHook => Object.freeze(CLIENT_METHODS.reduce(
  (client: UseClientHook & Record<string, UseClientHandler>, method: UseFetchMethod) => {
    client[method.toLowerCase()] = function useUseClientHandler<T>(
      uri: UseFetchUrl, originalOptions: Omit<UseFetchOptions, 'method'> | null,
      initialFetch: boolean = false
    ) {
      const clientUrl = (typeof uri === 'function' ? () => `${domain}${uri()}` : `${domain}${uri}`) as UseFetchUrl;
      const clientOptions = { ...(originalOptions || {}), method } as UseFetchOptions;

      const [fetchState, fetchDispatcher] = useFetch<T>(clientUrl, clientOptions, initialFetch);
      const clientDispatcher: any = (options: UseFetchOptions) => {
        const clientOptions = { ...options };
        clientOptions.method = method;
        fetchDispatcher(clientOptions);
      }

      return [fetchState, clientDispatcher];
    }

    return client;
  },
  {} as UseClientHook
))
