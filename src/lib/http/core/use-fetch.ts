import { useCallback, useEffect, useReducer } from 'react'

/**
 * Represents the possible actions that can be dispatched in the fetchReducer.
 * @template T - The type of data expected in the response.
 * @type {object} Action
 */
type Action<T> =
  | { type: 'loading' }
  | { type: 'fetched'; payload: T }
  | { type: 'error'; payload: Error }


/**
 * Represents the state of an HTTP request.
 * @template T - The type of data expected in the response.
 * @interface State
 * @property {T | undefined} data - The data received from the HTTP request.
 * @property {Error | undefined} error - An error object if the request encounters an error.
 */
export type UseFetchState<T> = { data?: T; error?: Error, loading: boolean }

export type UseFetchMethod = 'GET' | 'POST' | 'PUT' | 'DELETE';
export type FirstArgument<T extends abstract new (...args: any) => any> = T extends abstract new (...args: infer P) => any ? P[0] : never;
export type UseFetchUrl = `/${string}` | (() => `/${string}`);
export type UseFetchOptions = RequestInit & {
  queryParams?: FirstArgument<typeof URLSearchParams> | Record<string, number | boolean> | any,
  method: UseFetchMethod,
};


export function useFetch<T = unknown>(
  url: UseFetchUrl, originalOptions: UseFetchOptions,
  initialFetch: boolean = false,
): [UseFetchState<T>, (opt: UseFetchOptions) => void] {
  const initialState: UseFetchState<T> = {
    error: undefined,
    data: undefined,
    loading: false,
  }

  const fetchReducer = (state: UseFetchState<T>, action: Action<T>): UseFetchState<T> => {
    switch (action.type) {
      case 'loading': return { ...initialState, loading: true }
      case 'fetched': return { ...initialState, loading: false, data: action.payload }
      case 'error': return { ...initialState, loading: false, error: action.payload }
      default: return state
    }
  }

  const [fetchState, fetchStateReducer] = useReducer(fetchReducer, initialState)

  const fetchDispatcher = useCallback(async (options: UseFetchOptions) => {
    fetchStateReducer({ type: 'loading' })

    try {
      let targetUrl = typeof url === 'function' ? url() : url;
      if (options.queryParams || originalOptions.queryParams) {
        const searchParams: Record<string, any> = {}
        mapSearchIntoObj(originalOptions.queryParams, searchParams);
        mapSearchIntoObj(options.queryParams, searchParams);

        targetUrl += `?${new URLSearchParams(searchParams)}`;
      }

      const response = await fetch(targetUrl, { ...originalOptions, ...options })
      if (!response.ok) throw new Error(response.statusText)
      const data = (await response.json()) as T

      fetchStateReducer({ type: 'fetched', payload: data })
    } catch (error) {
      fetchStateReducer({ type: 'error', payload: error as Error })
    }
  }, [originalOptions, url]);

  useEffect(() => { initialFetch && fetchDispatcher(originalOptions) }, []);

  return [fetchState,
    (options: UseFetchOptions) => {
      fetchDispatcher(options)
    }
  ];
}

function mapSearchIntoObj(searchParamsArgs: FirstArgument<typeof URLSearchParams>, object: Record<string, any>) {
  new URLSearchParams(searchParamsArgs || undefined).forEach((value, key) => object[key] = value);
}
