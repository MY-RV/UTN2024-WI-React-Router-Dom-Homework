import { makeUseClient } from "./core/make-use-client";


export const useDJClient = makeUseClient('https://dummyjson.com');
